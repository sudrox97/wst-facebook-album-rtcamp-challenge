<?php
require_once '../config.php';
$logoutUrl = "http://localhost/wst-facebook-album-rtcamp-challenge/logout.php"
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Album example for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
</head>

<body>
    <header>
        <!--Replace this content with the User's profile photo-->
        <div class="collapse bg-dark" id="navbarHeader">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <h4 class="text-white">About</h4>
                        <p class="text-muted"> This website fetches your Facebook Albums and renders them in the
                            Slideshow.</p>
                    </div>
                    <div class="col-sm-4">
                        <!--Profile Picture-->
                        <a id="download_for_profile_pic">
                            <img id="profile-picture" onerror='this.src="images/blank.jpg"' class="img-circle">
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <h4 class="text-white">Contact</h4>
                        <ul class="list-unstyled">
                            <li><a href="#" class="text-white">Follow on Twitter</a></li>
                            <li><a href="#" class="text-white">Like on Facebook</a></li>
                            <li><a href="#" class="text-white">Email me</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar-dark bg-dark shadow-sm">
            <div class="container d-flex justify-content-between">
                <a href="#" class="navbar-brand d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="mr-2">
                        <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z">
                        </path>
                        <circle cx="12" cy="13" r="4"></circle>
                    </svg>
                    <strong>Facebook Albums</strong>
                </a>
                <a id="logout" href=<?php echo $logoutUrl; ?>>
                    <strong>Log Out</strong>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader"
                    aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </header>

    <main role="main">

        <section class="jumbotron text-center">

            <div id="slideshow" class="carousel slide" data-ride="carousel">
                <!-- slideshow -->
                <div class="carousel-inner active" id="fullscreen">
                    <img>
                    <button id="close" onclick="exitFullScreen()" style="display:none"><i
                            class="fa fa-close fa-3x"></i></button>
                    <div id="fullscreen-bar">
                        <button class="btn" id="toggle" onclick="fullScreen()" disabled><i
                                class="fas fa-expand fa-2x"></i></button>
                    </div>
                </div>

                <!-- Left and Right Controls -->
                <a class="carousel-control-prev" href="#slideshow" data-slide="prev" id="previous">
                    <span class="carousel-control-prev-icon" style="display:none"></span>
                </a>
                <a class="carousel-control-next" href="#slideshow" data-slide="next" id="next">
                    <span class="carousel-control-next-icon" style="display:none"></span>
                </a>

            </div>
        </section>

        <div class="album py-5 bg-light"></div>

    </main>

    <footer class="text-muted">
        <div class="container">
            <p class="float-right">
                <a href="#">Back to top</a>
            </p>
            <p>WST project based on the RTCamp Facebook Album Challenge <a
                    href="https://github.com/rtCamp/hiring-assignments/tree/master/Web-Developer">
                    Visit the homepage</a> or read our <a
                    href="https://gitlab.com/sudrox97/wst-facebook-album-rtcamp-challenge">
                    GitLab Source Code &copy;</a></p>
            <p> Collaborators: Shubham Sudame and Aditya Bhawalkar</p>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>

    <!--Link to my JavaScript custom code-->
    <script src="main.js"></script>
</body>

</html>