var albums, index, carousel_content, timeout_handler, selected_albums = [];
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        albums = JSON.parse(this.responseText);
        /* change the profile picture */
        var photo = getProfilePicture(albums);
        setProfilePicture(photo);

        /* Put photos in all the thumbnails*/
        var cover_photos = getCoverPhotos(albums);
        putCoverPhotos(cover_photos);
        downloadButtons();
        driveButtons();
    }
};
xhttp.open("GET", "http://localhost/wst-facebook-album-rtcamp-challenge/album.php", true);
xhttp.send();

function initiate(images) {
    index = 0;
    if (carousel_content == undefined) {
        carousel_content = document.querySelector(".carousel-inner img");
        carousel_content.src = images[0].source;
        carousel_content.style.background = "black";
    }
    var previous_button, next_button;
    previous_button = document.querySelector("#previous");
    next_button = document.querySelector("#next");

    changeImage(images);
    previous_button.onclick = function previous() {
        clearTimeout(timeout_handler);
        if (index > 1) {
            carousel_content.src = images[index - 2].source;
            index--;
        } else {
            index = images.length - 1;
            carousel_content.src = images[index].source;
        }
        timeout_handler = setTimeout(changeImage, 5000, images);
    };

    next_button.onclick = function next() {
        clearTimeout(timeout_handler);
        if (index < images.length - 1) {
            carousel_content.src = images[index + 1].source;
            index++;
        } else {
            carousel_content.src = images[0].source;
            index = 0;
        }
        timeout_handler = setTimeout(changeImage, 5000, images);
    };
}

function changeImage(images) {
    carousel_content.src = images[index].source;
    if (index < images.length - 1) {
        index++;
    } else {
        index = 0;
    }

    timeout_handler = setTimeout(changeImage, 5000, images);
}

function getProfilePicture(albums) {
    var profiles = function (albums) {
        for (var i = 0; i < albums.length; i++) {
            if (albums[i].name == "Profile Pictures") {
                return albums[i];
            }
        }
    }(albums);
    return profiles.photos[0].source;
}

function setProfilePicture(photo) {
    var frame = document.querySelector("#profile-picture");
    frame.src = photo;
    var download_image = document.querySelector("#download_for_profile_pic");
    download_image.href = photo;
    download_image.download = "profile.jpg";
}
/* Need to modify this one */
function getCoverPhotos(albums) {
    var cover_images = [];

    for (var i = 0; i < albums.length; i++) {
        var photos = albums[i].photos;
        var photo_source = albums[i].cover_photo;
        var album_id = getAlbumId(albums[i]);
        var album_name = getAlbumName(albums[i]);
        var photo_count = getPhotoCountInAlbum(albums[i]);
        var album_summary = {
            "id": album_id,
            "name": album_name,
            "count": photo_count,
            "source": photo_source
        };
        cover_images.push(album_summary);
        album_summary = null;
    }
    return cover_images;
}

function putCoverPhotos(cover_photos) {
    var form = document.createElement('form');
    form.action = "http://localhost/wst-facebook-album-rtcamp-challenge/action.php";
    form.method = "POST";
    var row = document.createElement('div');
    row.className = "row";

    for (var i = 0; i < cover_photos.length; i++) {
        var col_md_4 = document.createElement('div');
        col_md_4.className = "col-md-4";

        var card_mb_4_shadow_sm = document.createElement('div');
        card_mb_4_shadow_sm.className = "card mb-4 shadow-sm";

        var thumbnail_image = document.createElement('img');
        thumbnail_image.className = "card-img-top";
        thumbnail_image.src = cover_photos[i]["source"];
        thumbnail_image.alt = cover_photos[i]["name"];
        thumbnail_image.id = cover_photos[i]["id"];
        thumbnail_image.onclick = function () {
            play(this.id);
        };

        var card_body = document.createElement('div');
        card_body.className = "card-body";

        var description = document.createElement('div');
        description.className = "album_name";

        var album_name = document.createElement('p');
        album_name.className = "card-text";
        album_name.innerHTML = cover_photos[i]["name"];
        album_name.id = cover_photos[i]["id"];
        album_name.onclick = function () {
            play(this.id);
        }
        var checkbox_value = {
            name: cover_photos[i]["name"],
            urls: getPhotoUrls(getPhotosFromAlbum(albums, cover_photos[i]["id"]))
        };
        var album_checkbox = document.createElement('input');
        album_checkbox.type = "checkbox";
        album_checkbox.name = "album_checkbox[]";
        album_checkbox.className = "album_checkboxes";
        album_checkbox.value = JSON.stringify(checkbox_value);
        album_checkbox.checked = false;
        album_checkbox.onchange = function () {
            if (this.checked) {
                selected_albums.push(this.id);
                if (selected_albums.length > 1) {
                    var download_selected = document.querySelector("#download_selected");
                    download_selected.disabled = false;
                    var download_selected = document.querySelector("#move_selected");
                    download_selected.disabled = false;
                }
            } else {
                removeElement(selected_albums, this.id);
                if (selected_albums.length == 1 || selected_albums.length == 0) {
                    var download_selected = document.querySelector("#download_selected");
                    download_selected.disabled = true;
                    var download_selected = document.querySelector("#move_selected");
                    download_selected.disabled = true;
                }
            }
        };
        description.appendChild(album_name);
        description.appendChild(album_checkbox);

        var photo_count = document.createElement('p');
        photo_count.className = "card-text";
        photo_count.innerHTML = cover_photos[i]["count"];

        var d_flex = document.createElement('div');
        d_flex.className = "d-flex justify-content-between align-items-center";

        var btn_group = document.createElement('div');
        btn_group.className = "btn-group";

        var button2 = document.createElement('button');
        button2.type = "submit";
        button2.className = "btn btn-sm btn-outline-secondary";
        button2.id = cover_photos[i]['id'];
        button2.name = "album";
        button2.innerHTML = "Download This Album";
        var json = {
            name: cover_photos[i]["name"],
            urls: getPhotoUrls(getPhotosFromAlbum(albums, cover_photos[i]['id']))
        };
        button2.value = JSON.stringify(json);

        var button3 = document.createElement('button');
        button3.type = "submit";
        button3.className = "btn btn-sm btn-outline-secondary";
        button3.id = cover_photos[i]['id'];
        button3.name = "drive";
        button3.innerHTML = "Move";
        button3.value = JSON.stringify(json);

        btn_group.appendChild(button2);
        btn_group.appendChild(button3);

        d_flex.appendChild(btn_group);

        card_body.appendChild(description);
        card_body.appendChild(photo_count);
        card_body.appendChild(d_flex);

        card_mb_4_shadow_sm.appendChild(thumbnail_image);
        card_mb_4_shadow_sm.appendChild(card_body);

        col_md_4.appendChild(card_mb_4_shadow_sm);

        row.appendChild(col_md_4);
    }
    form.appendChild(row);
    var container = document.createElement('div');
    container.className = "container";
    container.appendChild(form);

    var thumbnails = document.querySelector(".album");
    thumbnails.appendChild(container);
}

function getAlbumsCount(albums) {
    return albums.length;
}

function getAlbumName(album) {
    return album.name;
}

function getPhotoCountInAlbum(album) {
    return album.count;
}

function getAlbumId(album) {
    return album.id;
}

function getPhotosFromAlbum(albums, id) {
    for (var i = 0; i < albums.length; i++) {
        if (albums[i].id == id) {
            return albums[i].photos;
        }
    }
}

function render(album_id) {
    var photos = getPhotosFromAlbum(albums, album_id);
    initiate(photos);
}

function play(id) {
    var toggle_button = document.querySelector("#toggle");
    toggle_button.disabled = false;

    var prev = document.querySelector('.carousel-control-prev-icon');
    prev.style.display = "inline-block";
    var next = document.querySelector('.carousel-control-next-icon');
    next.style.display = "inline-block";
    if (timeout_handler != undefined) {
        clearTimeout(timeout_handler);
        carousel_content.src = undefined;
    }
    render(id);
};

function downloadButtons() {
    var form = document.querySelector("form");
    var div = document.createElement('div');
    var br = document.createElement('br');

    var download_selected = document.createElement("button");
    download_selected.type = "submit";
    download_selected.className = "btn btn-success";
    download_selected.id = "download_selected";
    download_selected.name = "selected";
    download_selected.innerHTML = "Download Selected Albums";
    download_selected.disabled = true;

    var download_all = document.createElement("button");
    download_all.type = "submit";
    download_all.className = "btn btn-success";
    download_all.id = "download_all";
    download_all.name = "all";
    download_all.innerHTML = "Download All Albums";
    download_all.onclick = function () {
        checkAll('#' + this.id);
    };
    div.appendChild(download_selected);
    div.appendChild(download_all);
    form.appendChild(div);
    form.appendChild(br);
}

function driveButtons() {
    var form = document.querySelector("form");
    var div = document.createElement('div');
    var br = document.createElement('br');

    var move_selected = document.createElement("button");
    move_selected.type = "submit";
    move_selected.className = "btn btn-success";
    move_selected.id = "move_selected";
    move_selected.name = "drive_selected";
    move_selected.innerHTML = "Move Selected Albums";
    move_selected.disabled = true;

    var move_all = document.createElement("button");
    move_all.type = "submit";
    move_all.className = "btn btn-success";
    move_all.id = "move_all";
    move_all.name = "drive_all";
    move_all.innerHTML = "Move All Albums";
    move_all.onclick = function () {
        checkAll('#' + this.id);
    };
    div.appendChild(move_selected);
    div.appendChild(move_all);
    form.appendChild(div);
    form.appendChild(br)
}

function removeElement(array, element) {
    var index = array.indexOf(element);
    if (index > -1) {
        array.splice(index, 1);
    }
    return array;
}

function getPhotoUrls(photos) {
    var urls = [];
    for (var i = 0; i < photos.length; i++) {
        urls.push(photos[i].source);
    }
    return urls;
}

function checkAll(id) {
    var albums = [];
    var checkboxes = document.getElementsByClassName("album_checkboxes");
    for (var i = 0; i < checkboxes.length; i++) {
        albums.push(JSON.parse(checkboxes[i].value));
    }
    document.querySelector(id).value = JSON.stringify(albums);
}

function fullScreen() {
    var element = document.getElementById("fullscreen");
    element.classList.add("fullscreen");
    var close_button = document.querySelector("#close");
    close_button.style.display = "block";

    var prev = document.querySelector('#previous');
    prev.style.zIndex = "3";
    var next = document.querySelector('#next');
    next.style.zIndex = "3";
}

function exitFullScreen() {
    var element = document.getElementById("fullscreen");
    element.classList.remove("fullscreen");
    var close_button = document.querySelector("#close");
    close_button.style.display = "none";
    var prev = document.querySelector('#previous');
    prev.style.zIndex = "auto";
    var next = document.querySelector('#next');
    next.style.zIndex = "auto";
}