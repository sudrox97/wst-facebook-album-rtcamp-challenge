<?php
require_once 'config.php';

//validate access token
try {
    $accessToken = $helper->getAccessToken();
} catch (\Facebook\Exceptions\FacebookResponseException $e) {
    echo "Response Exception: " . $e->getMessage();
    exit();
} catch (\Facebook\Exceptions\FacebookSDKException $e) {
    echo "SDK Exception: " . $e->getMessage();
    exit();
}
if (!isset($accessToken)) {
    if ($helper->getError()) {
        header('HTTP/1.1 401 Unauthorized Access!');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
    } else {
        header('HTTP/1.1 400 Bad Request');
        echo "Bad Request";
    }
    exit;
}

//Successfully Logged in with the permissions. Now make a long-lasting access token
$oAuth2Client = $FB->getOAuth2Client();
//Get access token metadata
$tokenMetaData = $oAuth2Client->debugToken($accessToken);
// Validation (these will throw FacebookSDKException's when they fail)
$tokenMetaData->validateAppId('2277983128920343');
$tokenMetaData->validateExpiration();

if (!$accessToken->isLongLived()) {
    //Exchanges a short-lived access token for a long-lived one
    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
}
/* User is logged in with a long-lived access token */
$_SESSION['accessToken'] = (string) $accessToken;
/* Now that the user is logged in, and access token is made, show him the page.*/
header('Location: http://localhost/wst-facebook-album-rtcamp-challenge/album/layout.php');
exit();