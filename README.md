# WST-Facebook-Album-RTCamp-Challenge

Web Systems & Technologies Project. The script manages users to view slideshow of albums present on user's Facebook account. Albums should also be downloadable and backed up to the users' Google Drive.

login.php:
PHP Script that generates the login link.

fb-callback.php:
The Callback URL that Facebook redirects user to after login dialog. Any valid URL is in the Valid OAuth2 redirect URIs list. 

TODO List:
1. Once album photos are loaded into the slideshow, then start a timer, so that after some interval, the photos will keep changing.
2. Change the icons of the Play, Download and Save to Drive Buttons. Remove the text description. Put that description as tooltip.
3. If user is already logged in via his Facebook account, till the access token is valid, don't make him lose data after refresh.
4. For albums with more than 100 photos, work with the paging.
5. Make Full Screen for Slideshow.

Installations:
For Download Features, make sure you have installed zip feature with PHP.
sudo apt-get install php7.2-zip
