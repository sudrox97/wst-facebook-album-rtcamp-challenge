<?php
require_once 'zip.php';
/* Download Single Album */
function downloadSingleAlbum($data)
{
    $album = json_decode($data, true);
    $name = $album["name"];
    $urls = $album["urls"];
    $temp_dir = "/tmp" . "/" . $name;
    if (!is_dir($temp_dir)) {
        mkdir($temp_dir);
    }
    downloadImagesToFolder($urls, $temp_dir);
    sendZip($temp_dir);
}
/* Download Selected Albums */
function downloadSelectedAlbums($data)
{
    if (!empty($data)) {
        $albums = $data;
        $super = "/tmp" . "/" . date('m-d-Y_H:i:s');
        if (!is_dir($super)) {
            mkdir($super);
        }
        foreach ($albums as $album) {
            $json = json_decode($album, true);
            $folder_name = $json["name"];
            $urls = $json["urls"];
            downloadImagesToFolder($urls, $super . '/' . $folder_name);
        }
        sendZip($super);
    }
}
/* Download All Albums */
function downloadAllAlbums($data)
{
    $super = "/tmp" . "/" . date('m-d-Y_H:i:s');
    if (!is_dir($super)) {
        mkdir($super);
    }
    $albums = json_decode($data, true);
    foreach ($albums as $album) {
        $folder_name = $album['name'];
        $urls = $album['urls'];
        downloadImagesToFolder($urls, $super . '/' . $folder_name);
    }
    sendZip($super);
}

function downloadImagesToFolder($urls, $directory)
{
    if (!(is_dir($directory))) {
        mkdir($directory);
    }
    foreach ($urls as $url) {
        $tmp_file = tempnam("$directory/", time());
        $file = $tmp_file . '.jpg';
        $content = file_get_contents($url);
        file_put_contents($file, $content);
        unlink($tmp_file);
    }
}

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . "/" . $object)) {
                    rrmdir($dir . "/" . $object);
                } else {
                    unlink($dir . "/" . $object);
                }

            }
        }
        rmdir($dir);
    }
}

function sendZip($temp_dir)
{
    $delimiter = "/tmp" . "/";
    $name = explode($delimiter, $temp_dir)[1];
    $zipper = new Zip;
    $zipPath = $temp_dir . '.zip';
    $zip = $zipper->zipDir($temp_dir, $zipPath);
    header("Content-type: application/zip");
    header("Content-disposition: attachment; filename=$name.zip");
    readfile($zipPath);
    unlink($zipPath);
    rrmdir($temp_dir);
}