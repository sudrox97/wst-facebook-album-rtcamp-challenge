<?php
require_once 'google-api-php-client/vendor/autoload.php';
require_once 'google-api-php-client/src/Google/Client.php';
require_once 'google-api-php-client/vendor/google/apiclient-services/src/Google/Service/Drive.php';
require_once 'google-api-php-client/vendor/google/apiclient-services/src/Google/Service/Oauth2.php';
session_start();
if ($_POST) {
    $_SESSION["post"] = $_POST;
} else {
    $_POST = $_SESSION["post"];
}
global $service, $created;
$secret = json_decode(file_get_contents('client_secrets.json'), true);
$client = new Google_Client();
$client->setClientId($secret["web"]["client_id"]);
$client->setClientSecret($secret["web"]["client_secret"]);
$client->setRedirectUri($secret["web"]["redirect_uris"][0]);
$client->addScope(
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/userinfo.email'
);
$client->setAccessType('offline');
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
    $client->setAccessToken($_SESSION['access_token']);
    if ($client->isAccessTokenExpired()) {
        $client->refreshToken($_SESSION['access_token']['refresh_token']);
    }
} else {
    if (!isset($_GET['code'])) {
        $auth_url = $client->createAuthUrl();
        header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
        exit();
    } else {
        $client->authenticate($_GET['code']);
        $_SESSION['access_token'] = $client->getAccessToken();
    }
}
$service = new Google_Service_Drive($client);
/* This folder is created only once.*/
$master_folder_name = "Facebook Albums";
$masterFolderMIMEType = "application/vnd.google-apps.folder";
deleteFolderIfExists($service, $master_folder_name);
$master_folder = new Google_Service_Drive_DriveFile();
$master_folder->setName($master_folder_name);
$master_folder->setMimeType($masterFolderMIMEType);
$created = $service->files->create($master_folder);

moveAction();
header("Location: http://localhost/wst-facebook-album-rtcamp-challenge/album/layout.php");
exit();
function moveAction()
{
    if (isset($_POST["drive"])) {
        $album = json_decode($_POST["drive"], true);
        moveSingleAlbum($album);
    }
    if (isset($_POST["drive_selected"])) {
        $albums = $_POST["album_checkbox"];
        foreach ($albums as $album) {
            $data = json_decode($album, true);
            moveSingleAlbum($data);
        }
    }
    if (isset($_POST["drive_all"])) {
        $albums = json_decode($_POST["drive_all"], true);
        foreach ($albums as $album) {
            moveSingleAlbum($album);
        }
    }
}
function moveSingleAlbum($album)
{
    $name = $album["name"];
    $urls = $album["urls"];
    $created = $GLOBALS["created"];
    $service = $GLOBALS["service"];
    $folder_mime = "application/vnd.google-apps.folder";
    $folder_parentId = $created->id;

    $folder = new Google_Service_Drive_DriveFile();
    $folder->setName($name);
    $folder->setMimeType($folder_mime);
    $folder->setParents(array($folder_parentId));

    $sub = $service->files->create($folder);
    addFilesToFolder($service, $sub->id, $urls);
}

function addFilesToFolder($service, $parentId, $urls)
{
    foreach ($urls as $url) {
        $file = new Google_Service_Drive_DriveFile();
        $file_name = uniqid() . '.jpg';
        $file->setName($file_name);
        $file->setParents(array($parentId));
        $service->files->create($file,
            array(
                'data' => file_get_contents($url),
                'mimeType' => 'image/jpg',
                'uploadType' => 'media',
            )
        );
    }
}

function getFolderId($service, $folder_name)
{
    $parameters['q'] = "mimeType='application/vnd.google-apps.folder' and trashed=false";
    $files = $service->files->listFiles($parameters);
    $files = $files["files"];
    foreach ($files as $file) {
        if ($file['name'] == $folder_name) {
            return $file['id'];
        }
    }
    return null;
}

function deleteFolderIfExists($service, $folder_name)
{
    $folder_id = getFolderId($service, $folder_name);
    if ($folder_id != null) {
        $deleted = $service->files->delete($folder_id);
        if ($deleted) {
            return true;
        }
    }
    return false;
}