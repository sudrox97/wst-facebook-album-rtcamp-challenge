<?php
//Get the required fields from user's Facebook page.
function getAlbums($access_token)
{
    $fields = "id,name,count,cover_photo{source}";
    $graph_album_link = "https://graph.facebook.com/v3.2/me/albums?fields={$fields}&access_token={$access_token}";

    $albums = array();

    $album_list = json_decode(file_get_contents($graph_album_link), true);
    for ($i = 0; $i < count($album_list['data']); $i++) {
        $album = array();
        $album_photos = array();
        $album_id = $album_list['data'][$i]['id'];
        $album_photos_count = $album_list['data'][$i]['count'];
        $after = null;
        while ($album_photos_count > 0) {
            $graph_link = "https://graph.facebook.com/v3.2/{$album_id}/photos?fields=source&after={$after}&access_token={$access_token}";

            $photos = json_decode(file_get_contents($graph_link), true);
            $album_photos = appendArray($album_photos, $photos['data']);
            $count = count($photos['data']);
            $album_photos_count -= $count;
            $after = $photos['paging']['cursors']['after'];
        }
        $album['id'] = $album_id;
        $album['name'] = $album_list['data'][$i]['name'];
        $album['count'] = $album_list['data'][$i]['count'];
        $album['cover_photo'] = $album_list['data'][$i]['cover_photo']['source'];
        $album['photos'] = $album_photos;
        array_push($albums, $album);
    }

    $jsonData = json_encode($albums);
    return $jsonData;
}

function appendArray($array, $subarray)
{
    for ($i = 0; $i < count($subarray); $i++) {
        array_push($array, $subarray[$i]);
    }
    return $array;
}