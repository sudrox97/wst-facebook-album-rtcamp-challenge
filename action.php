<?php

if (isset($_POST["album"]) || isset($_POST["selected"]) || isset($_POST["all"])) {
    require_once 'download.php';
    if (isset($_POST["album"])) {
        downloadSingleAlbum($_POST["album"]);
    }
    if (isset($_POST["selected"])) {
        downloadSelectedAlbums($_POST["album_checkbox"]);
    }
    if (isset($_POST["all"])) {
        downloadAllAlbums($_POST["all"]);
    }
}
if (isset($_POST["drive"]) || isset($_POST["drive_selected"]) || isset($_POST["drive_all"])) {
    require_once 'drive.php';
}
exit();